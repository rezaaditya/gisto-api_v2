const elixir = require('laravel-elixir');

require('laravel-elixir-vue');
require('laravel-elixir-webpack-official')

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss')
       .webpack('app.js');

    /********************/
    /* Copy Stylesheets */
    /********************/
    mix.styles(
    [
    './bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css',
    './bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css',
    './bower_components/sweetalert2/dist/sweetalert2.min.css',
    './bower_components/gritcode-components/dist/gritcode-components.min.css',
    './bower_components/select2/dist/css/select2.min.css',
    './bower_components/animate.css/animate.css',
    './bower_components/gentelella/build/css/custom.min.css',
    ],
    'public/bower_components/css/all.css'
    );

    /****************/
    /* Copy Scripts */
    /****************/

    mix.scripts(
    [
    './bower_components/gentelella/vendors/jquery/dist/jquery.min.js',
    './bower_components/gentelella/production/js/moment/moment.min.js',
    './bower_components/es6-promise/promise.min.js',
    './bower_components/gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    './bower_components/bootstrap/dist/js/bootstrap.min.js',
    './bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
    './bower_components/gentelella/build/js/custom.min.js',
    './bower_components/es6-promise/promise.min.js',
    './bower_components/sweetalert2/dist/sweetalert2.min.js',
    './bower_components/select2/dist/js/select2.min.js',
    './bower_components/bower-jquery-sparkline/dist/jquery.sparkline.retina.js',
    //    'app.js',
    ],
    'public/bower_components/js/all.js'
    );

    /**************/
    /* Copy Fonts */
    /**************/

    // Bootstrap
    mix.copy('./bower_components/gentelella/vendors/bootstrap/fonts/', 'public/bower_components/fonts');
    mix.copy('./bower_components/ckeditor', 'public/bower_components/ckeditor');

    // Font awesome
    mix.copy('./bower_components/gentelella/vendors/font-awesome/fonts/', 'public/bower_components/fonts');
});
