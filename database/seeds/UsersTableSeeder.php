<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'albertjulian97@gmail.com',
            'password' => bcrypt('testing')
        ]);
        User::create([
            'email' => 'admin@lazato.com',
            'password' => bcrypt('admin')
        ]);
    }
}
