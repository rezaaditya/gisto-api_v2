<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveHashtagNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('hashtag_news');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('hashtag_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id');
            $table->integer('hashtag_id');
            $table->timestamps();
        });
    }
}
