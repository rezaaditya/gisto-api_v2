<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHashtaggablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hashtaggables', function (Blueprint $table) {
            // $table->increments('id');;
            $table->integer('hashtag_id')->unsigned();
            $table->integer('hashtaggable_id')->unsigned();
            $table->string('hashtaggable_type');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hashtaggables');
    }
}
