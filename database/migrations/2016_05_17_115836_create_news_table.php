<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('content');

            $table->string('quote')->nullable();
            $table->string('author')->nullable();

            $table->string('shift');
            $table->string('date');

            $table->integer('category_id')->nullable();
            $table->integer('meta_id')->default(0);
            $table->string('is_draft')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
