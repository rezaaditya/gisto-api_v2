<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome2');
});

Route::group(['prefix' => 'admin'], function() {
    Route::get('logout','Auth\LoginController@logout');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('login', 'Auth\LoginController@showLoginForm');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::resource('library', 'LibraryController');
    Route::group(['prefix' => 'news'], function() {
        Route::get('curate', 'NewsController@curate');
        Route::get('digest', 'NewsController@digest');
        Route::resource('/', 'NewsController');
    });
    Route::group(['prefix' => 'topic'], function() {
        Route::get('/', 'TopicController@index');
    });
    Route::group(['prefix' => 'category'], function() {
        Route::get('/', 'CategoryController@index');
    });

    Route::group(['prefix' => 'api'], function() {
        Route::get('/', 'ApiController@index');
    });

    Route::group(['prefix' => ''], function() {
        Route::get('/', 'DashboardController@index');
    });
});
