<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Notes
 *
 * Struktur groupnya yang rapi yaa
 */

Route::group(['namespace' => 'Api', 'prefix' => 'auth'], function(){
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@authenticate');
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('jwt-auth');

Route::group(['namespace' => 'Api'], function() {

    Route::group(['middleware' => ['jwt-auth']], function() {
        Route::get('user/{id}','UserController@show');
        Route::post('user/{id}/request','FriendController@request');
        Route::post('user/{id}/accept-friend-request','FriendController@accept');
        Route::post('user/{id}/decline','FriendController@decline');
        Route::post('user/{id}/unfriend','FriendController@unfriend');
    });

    Route::post('news/{id}/comment','NewsController@comment');
    Route::resource('news', 'NewsController');
    Route::resource('topic', 'TopicController');
    Route::resource('category', 'CategoryController');
    Route::resource('library', 'LibraryController');

});
