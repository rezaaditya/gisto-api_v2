<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\FileManagement;
use App\Library;
use Image;
use App\Libraries\Help;
use \Carbon;
use Illuminate\Support\Facades\DB;

class LibraryController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->model=new Library;
    }

    public function index()
    {
        $data = $this->model->get();
        return view('admin.library.index', compact('data'));
    }

    public function create()
    {
        return view('admin.library.create');
    }

    public function store()
    {
        $data = $request->all();
        $library = $this->model->create($data);

    }

    public function show()
    {
    }

    public function edit()
    {
        return view('admin.library.edit');
    }

    public function update()
    {
    }

    public function destroy()
    {
    }
}
