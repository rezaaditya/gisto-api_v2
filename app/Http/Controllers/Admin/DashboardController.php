<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\News;
use App\Topic;
use App\Library;
use App\Category;
use App\Hashtag;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
    	$top_hashtag=[];
    	$top_hashtag=Hashtag::orderBy('count','desc')->limit(5)->get();

    	$data=[];
    	$data['news_count'] = News::all()->count();
    	$data['topic_count'] = Topic::all()->count();
    	$data['library_count'] = Library::all()->count();
    	$data['category_count'] = Category::all()->count();
    	$data['hashtag_count'] = Hashtag::all()->count();
        return view('admin.dashboard.index', compact('data','top_hashtag'));
    }
}
