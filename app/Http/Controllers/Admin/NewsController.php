<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;
use App\Category;
use App\FileManagement;
use App\Statistic;
use App\Author;
use App\Voting;
use App\VotingOption;
use Image;
use App\Libraries\Help;
use \Carbon;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->model=new News;
    }
    public function index(){
        $data=$this->model->get()->groupBy('date')->transform(function($item, $k) {
            return $item->groupBy('shift');
        });
        $data=collect([]);
        foreach (range(0,7) as $row) {
            $date=Carbon\Carbon::today()->addDays($row);
            $data->put(
                $date->format('j F Y'),
                ['morning'=>collect([
                    'date' => $date->toDateString(),
                    'shift' => 'morning',
                    'badge-content' => $this->model
                    ->whereDate('date','=',$date->toDateString())
                    ->where('shift', 'morning')
                    ->count().' / 6',
                    'icon' => 'fa-sun-o',
                    'title' => 'Morning Digest',
                ]),
                'evening'=>collect([
                    'date' => $date->toDateString(),
                    'shift' => 'evening',
                    'badge-content' => $this->model
                    ->whereDate('date','=',$date->toDateString())
                    ->where('shift', 'evening')
                    ->count().' / 6',
                    'icon' => 'fa-moon-o',
                    'title' => 'Evening Digest',
                ])]
            );
        }
        return view('admin.news.index', compact('data'));
    }
    public function digest(Request $request){

        $data=$this->model
        ->whereDate('date','=',$request->get('date'))
        ->where('shift', $request->get('shift'))
        ->get();
        $request=$request->all();
        $count=$data->count();
        return view('admin.news.digest', compact('data', 'request', 'count'));
    }

    // TODO bikin curate
    public function curate(Request $request){
        $data=range(0,20);
        return view('admin.news.curate', compact('data', 'request'));
    }

    public function create(Request $request)
    {
        $date=$request->get('date');
        $shift=$request->get('shift');
        $categories=Category::select(DB::raw('id as value, name as label'))->get();
        return view('admin.news.create', compact('categories', 'date', 'shift'));
    }

    public function store(Request $request){
        $data = $request->all();
        // dd($data);
        $news=News::create($data);

        if ($request->hasFile('image-main')) {
            $path='images/news';
            $file = $request->file('image-main');

            $image= $news->files()->create([
                'attachment_type_id' => 1,
                'name' => '',

                'type' => 'cover',
                'is_online' => 0,
                'file_type' => 'image',

                'base_url' => $path.'/'.$news->id.'/',
                'file_name' => $news->id.'_'.date('Y-m-d'),
                'extension' => $file->getClientOriginalExtension(),
            ]);
            $dir=$image->base_url;
            if (!is_dir($dir)) {
                mkdir($dir, 755, true);
            }
            $full_path=$dir.$image->file_name.'.'.$image->extension;
            Image::make($file)->fit(612, 612)->save($full_path);

            $full_path_thumb=$dir.$image->file_name.'_thumb.'.$image->extension;
            Image::make($file)->fit(200, 200)->save($full_path_thumb);
        }

        if ($request->has('video_url')) {
            $image= $news->files()->create([
                // 'name' => $row['name'],

                'type' => 'gallery',
                'is_online' => 1,
                'file_type' => 'video',

                'base_url' => $data['video_url'],
            ]);
        }
        if ($request->has('gallery')) {
            $gallery = $data['gallery'];
            foreach($gallery as $name => $row){
                if($row['is_online']==0){
                    $file=$row['src'];
                    $name=$row['name'];
                    $path='images/news/'.$news->id;
                    $image= $news->files()->create([]);
                    $image->update([
                        'attachment_type_id' => 1,
                        'name' => $name,

                        'type' => 'gallery',
                        'is_online' => $row['is_online'],
                        'file_type' => 'image',

                        'base_url' => $path.'/',
                        'file_name' => $name.'_'.$image->id.'_'.date('Y-m-d'),
                        'extension' => $file->getClientOriginalExtension(),
                    ]);
                    $configs=[
                        ['name' => 'thumb', 'size' => [80,80]],
                        ['name' => 'small', 'size' => [320,240]],
                        ['name' => 'medium', 'size' => [640,480]],
                        ['name' => 'normal', 'size' => null],
                    ];
                    foreach($configs as $config){
                        $dir=$image->base_url.$config['name'];
                        if (!is_dir($dir)) {
                            mkdir($dir, 755, true);
                        }
                        $path_resized=$dir.'/'.$image->file_name.'.'.$image->extension;
                        if($config['size']!=null){
                            Image::make($file)->fit($config['size'][0], $config['size'][1])->save($path_resized);
                        }
                        else{
                            Image::make($file)->save($path_resized);
                        }
                    }
                }
                else{
                    $image= $news->files()->create([
                        'name' => $name=$row['name'],

                        'type' => 'gallery',
                        'is_online' => $row['is_online'],
                        'file_type' => 'image',

                        'base_url' => $row['base_url'],
                    ]);
                }

            }
        }
        // foreach($data["hashtags"] as $row){
        //     if(is_numeric($row)){
        //         $news->hashtags()->attach($row);
        //     }
        //     else{
        //         $news->hashtags()->create([
        //             "name"=> $row
        //         ]);
        //     }
        // }

        $news->statistics()->create($data["statistic"]);

        $voting=$news->voting()->create($request->get('voting'));
        foreach ($data["voting"]["options"] as $row) {
            $voting->options()->create($row);
        }

        if($request->has('links')){
            foreach ($data["links"] as $row) {
                $news->depth_links()->create($row);
            }
        }

        return redirect()->action('Admin\NewsController@digest', [
            'date' => $request->get('date'),
            'shift' => $request->get('shift')
        ])->with('success','add');
    }

    public function edit($id){
        return view('admin.news.edit');
    }

    public function destroy($id){
        $this->model->find($id)->delete();
        $code=200;
        $response['status']='success';
        return response()->json($response, $code);
    }
}
