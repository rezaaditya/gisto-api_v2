<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      date_default_timezone_set('Asia/Jakarta');
      Carbon::setLocale('id');
      $this->model=new User;
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=null;
        $response=[];
        if($data=$this->model->find($id)){
            $response['data']=$data;
            $response['status']='success';
        }
        else{
            $response['status']='error';
            $response['message']='unregister_tick_function not found';
        }

        return response()->json($response);
    }
}
