<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use Carbon\Carbon;

class CategoryController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      date_default_timezone_set('Asia/Jakarta');
      Carbon::setLocale('id');
      $this->model=new Category;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      $response=[];
      $data = $this->model->all();

      $response['data']=$data;
      $response['count']=$data->count();
      $response['status']='success';
      return response()->json($response, 200);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $data=null;
      $response=[];
      if($data=$this->model->find($id)){
          $response['data']=$data;
          $response['status']='success';
      }
      else{
          $response['status']='error';
          $response['message']='News not found';
      }

      return response()->json($response);
  }


  public function store(Request $request)
  {
      $data=$this->model->create($request->all());
      $response['data']=$data;
      $response['status']='success';

      return response()->json($response);
  }
  public function update(Request $request, $id)
  {
      $data=$this->model->find($id);
      $data->update($request->all());
      $response['data']=$data;
      $response['status']='success';

      return response()->json($response);
  }

  public function destroy($id){
      $this->model->find($id)->delete();
      $code=200;
      $response['status']='success';
      return response()->json($response, $code);
  }
}
