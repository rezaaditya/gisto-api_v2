<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Libraries\Help;
use Response;

use DB;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        Carbon::setLocale('id');
        $this->model=new News;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response=[];
        $data = $this->model
        ->orderBy('date', 'desc')
        ->orderBy('id', 'desc')
        ->with(
            'depth_links',
            'category',
            'hashtags',
            'voting'
        );

        if($request->has('date')){
            $data->whereDate('date','=',$request->get('date'))
            ->where('shift',$request->get('shift'));
        }
        $response['data']=$data->take(6)->get();
        $response['count']=$response['data']->count();
        $response['status']='success';
        return Response::json($response,200);
        // return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=null;
        $response=[];
        if($data=$this->model->find($id)){
            $response['data']=$data;
            $response['status']='success';
        }
        else{
            $response['status']='error';
            $response['message']='News not found';
        }

        return response()->json($response);
    }

    public function destroy($id){
        // $this->model->find($id)->delete();
        // $code=200;
        // $response['status']='success';
        // return response()->json($response, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment($id)
    {
        $data=null;
        $response=[];
        if($data=$this->model->find($id)){

            $response['data']=$data;
            $response['status']='success';
        }
        else{
            $response['status']='error';
            $response['message']='News not found';
        }

        return response()->json($response);
    }
}
