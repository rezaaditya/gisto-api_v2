<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index(){
        $data=[
            [
                "name"=>"tomo",
                "age"=>"6",
            ],
            [
                "name"=>"tommy",
                "age"=>"9",
            ],
        ];
        return response()->json($data);
    }
}
