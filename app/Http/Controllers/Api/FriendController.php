<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FriendController extends Controller
{
    public function request($id)
    {
        $sender = User::findOrFail(Auth::user()->id);
        $receiver = User::findOrFail($id);
        $sender->befriend($receiver);
        $response['status'] = 'success';
        return response()->json($response);
    }

    public function accept($id)
    {
        $sender = User::findOrFail(Auth::user()->id);
        $receiver = User::findOrFail($id);
        $recipient->acceptFriendRequest($sender);
        $response['status'] = 'success';
        return response()->json($response);
    }

    public function decline($id)
    {
        $receiver = User::findOrFail(Auth::user()->id);
        $sender = User::findOrFail($id);
        $receiver->denyFriendRequest($sender);
        $response['status'] = 'success';
        return response()->json($response);
    }

    public function unfriend($id)
    {
        $sender = User::findOrFail(Auth::user()->id);
        $receiver = User::findOrFail($id);
        $sender->unfriend($recipient);
        $response['status'] = 'success';
        return response()->json($response);
    }
}
