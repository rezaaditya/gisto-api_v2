<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'image',
        'count',
    ];

	public function getCountAttribute(){
        return $this->news()->count();
    }
    public function files()
    {
        return $this->morphMany('App\FileManagement', 'fileable');
    }

	public function getImageAttribute(){
        if ($d=$this->files()->first()) {
            return $d;
        }
        return null;
    }

    public function file_management()
    {
        return $this->belongsTo('App\FileManagement');
    }
    public function news(){
        return $this->hasMany('App\News');
    }
}
