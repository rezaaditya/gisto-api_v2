<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingOption extends Model
{
     protected $fillable = [
         'voting_id', 'answer'
     ];
}
