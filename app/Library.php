<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    protected $fillable = [
        'title',
        'content',
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function imagemain(){
        return $this->belongsTo('App\FileManagement','file_management_id','id');
    }
    public function image1(){
        return $this->belongsTo('App\FileManagement','image1_id','id');
    }
    public function image2(){
        return $this->belongsTo('App\FileManagement','image2_id','id');
    }
    public function image3(){
        return $this->belongsTo('App\FileManagement','image3_id','id');
    }
    public function video(){
        return $this->belongsTo('App\FileManagement','image3_id','id');
    }
    public function author(){
        return $this->belongsTo('App\Author');
    }
    public function statistic(){
        return $this->belongsTo('App\Statistic');
    }
    public function depth_links()
    {
        return $this->hasMany('App\DepthLink');
    }


    /**
     * Get all of the owning imageable models.
     */
    public function newsable()
    {
        return $this->morphTo();
    }

    /**
     * Get all of the layout contents that are assigned this news.
     */
    public function layoutContents()
    {
        return $this->morphedByMany('App\LayoutContent', 'libraryable');
    }

    /**
     * Get all of the layout pictures that are assigned this tag.
     */
    public function layoutPictures()
    {
        return $this->morphedByMany('App\LayoutPicture', 'libraryable');
    }

    /**
     * Get all of the layout quotes that are assigned this tag.
     */
    public function layoutQuotes()
    {
        return $this->morphedByMany('App\LayoutQuote', 'libraryable');
    }
}
