<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LayoutQuote extends Model
{

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
     'author',
     'content',
  ];

  protected $type="quote";
  protected $position;
  protected $appends = ['type', 'position'];


  public function getTypeAttribute()
  {
   return $this->type;
  }
  public function getPositionAttribute()
  {
   return $this->newsable->first()->position;
  }

  public function newsable()
  {
    return $this->morphMany('App\Newsables', 'newsable');
  }

  public function news()
  {
    return $this->morphToMany('App\News', 'newsable');
  }

   public function library()
   {
      return $this->morphToMany('App\Library', 'libraryable');
   }
}
