<?php

namespace App\Services;

use Summarizer\Summarizer;

class Summarize{

	public static function tempo_filter($url){
        $url = substr($url, 4);
        $url = "https".$url;
		$ambilhtml = file_get_contents($url);
        $filter = explode('<!-- end block display -->', $ambilhtml);
        $filter = explode('<!-- end artikel -->', $filter[1]);

        // $filter = explode('<p>', $filter[0]);
/*          $filter = explode('<strong>', $filter[0]);
        $filter = strip_tags($filter[count($filter)-2]);*/
        //$filter = preg_replace("/&#?[a-z0-9]+;/i","",$filter);
        // $filter = preg_replace(array("\r", "\n", "\t"),"",$filter);

        // to strip all tags and wrap italics with underscore
/*            strip_tags(str_replace(array("<i>", "</i>"), array("_", "_"), $filter[0]));
        strip_tags(str_replace(array("<p>", "</p>"), array("_", "_"), $filter[0]));
        strip_tags(str_replace(array("<strong>", "</strong>"), array("_", "_"), $filter[0]));

        // to preserve anchors...
        str_replace("|a", "<a", strip_tags(str_replace("<a", "|a", $filter[0])));*/
        $filter = preg_replace( "/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($filter[0]))));
        return trim($filter);
    }

    public static function cleanJSON($text)
    {
       return preg_replace('!\\r?\\n!', "", $text);
    }

    public static function contains($needle, $haystack)
    {
	    return stripos($haystack, $needle) !== false;
	}

	public static function contains_array($needles, $haystack)
	{
		foreach ($needles as $needle) {
			if(!Help::contains($needle, $haystack))
			{
				return false;
			}
		}
	    return true;
	}

    //TODO summary from tempo rss
    public static function summary($key){
        $url='https://rss.tempo.co/index.php/teco/news/feed/start/0/limit/50';
        $key=explode(" ",$key);
        //get all data from rss feed
        $data=[];
        $xml=simplexml_load_file($url,'SimpleXMLElement', LIBXML_NOCDATA);
        foreach ($xml->channel->item as $row) {
            if(Help::contains_array($key,trim($row->title))){
                $item=[
                    'title' => trim($row->title),
                    'link' => trim($row->link),
                    'date' => trim($row->pubDate),
                    'description' => trim($row->description),
                ];
                array_push($data,$item);
            }
        }
        // TODO get 5 top article from search by keyword
        $data = array_slice($data, 0,4);
        //TODO filter rss by keywords

        $articles="";
        foreach ($data as $row) {
            $articles .= " ".Help::tempo_filter($row['link']);
        }
        $sum =  new Summarizer();

        return $sum->summarize($articles);
    }

    //TODO search keywords from tempo
    public static function key($key)
    {
        $url='https://rss.tempo.co/index.php/teco/news/feed/start/0/limit/50';
        $key=explode(" ",$key);
        //get all data from rss feed
        $data=[];
        $xml=simplexml_load_file($url,'SimpleXMLElement', LIBXML_NOCDATA);
        foreach ($xml->channel->item as $row) {
            if(Help::contains_array($key,trim($row->title))){
                return true;
            }
        }
    }

    //TODO count how much html tag
    public static function count($htmlTag,$text)
    {
        return substr_count($text, $htmlTag);
    }

    //TODO break the sentences
    public static function breakSentences($htmlTag,$text)
    {
        $sentences = explode($htmlTag, $text);
        $merge[0]["paragraph"] = "";
        $merge[1]["paragraph"] = "";
        $count = 0;
        foreach($sentences as $row){
            if ($count <=4) {
                $merge[0]["paragraph"] .= "<p>".$row;
                $count++;
            }else{
                $merge[1]["paragraph"] .= "<p>".$row;
            }
        }
        return $merge;
    }

/*		public function show($id)
	{
		//TODO summary from keyword
		$articles = "";
		for ($i=0; $i < count(Help::summary($id)); $i++) {
			$articles .= "<br><br>".Help::summary($id)[$i];
		}
		//TODO search keyword
		// return var_dump(Help::key($id));
		return $articles;
	}*/

}
