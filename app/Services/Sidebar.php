<?php

namespace App\Services;

class Sidebar{

    public function get(){
        $sidebar=[
            'title'=>'Gisto',
            'items'=>[
                [
                    'header' => 'Admin',
                    'items' => [
                        [
                            'name' => 'Home',
                            'fa-icon' => 'fa-home',
                            'controller' => 'Admin\DashboardController@index',
                        ],
                        [
                            'name' => 'News',
                            'fa-icon' => 'fa-newspaper-o',
                            'controller' => 'Admin\NewsController@index',
                        ],
                        [
                            'name' => 'Category',
                            'fa-icon' => 'fa-cubes',
                            'controller' => 'Admin\CategoryController@index',
                        ],
                        [
                            'name' => 'Library',
                            'fa-icon' => 'fa-files-o',
                            'controller' => 'Admin\LibraryController@index',
                        ],
                        [
                            'name' => 'Topic',
                            'fa-icon' => 'fa-ticket',
                            'controller' => 'Admin\TopicController@index',
                        ],
                        [
                            'name' => 'Api',
                            'fa-icon'=> 'fa-exchange',
                            'controller' => 'Admin\ApiController@index'
                        ]
                        // [
                        //     'name' => 'Topic',
                        //     'fa-icon' => 'fa-home',
                        //     'sub_items' => [
                        //         [
                        //             'name' =>'Topic List',
                        //             'controller' =>'Admin\TopicController@index',
                        //         ],
                        //         [
                        //             'name' =>'Create Topic',
                        //             'controller' =>'Admin\TopicController@index',
                        //         ],
                        //     ]
                        // ],

                    ],
                ],
                [
                    'header' => 'Header 1',
                    'items' => [
                        [
                            'name' => 'Rumahku',
                            'fa-icon' => 'fa-home',
                            'sub_items' => [
                                [
                                    'name' =>'home-1',
                                    'controller' =>'Admin\NewsController@index',
                                ],
                                [
                                    'name' =>'home-1',
                                    'controller' =>'Admin\NewsController@index',
                                ],
                            ]
                        ],
                        [
                            'name' => 'Home',
                            'fa-icon' => 'fa-home',
                            'controller' => 'Admin\NewsController@index',
                        ],
                    ],
                ],
            ],
        ];
        return $sidebar;
    }
}
