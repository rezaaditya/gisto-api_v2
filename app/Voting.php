<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
     protected $fillable = [
         'question'
     ];

     public function options(){
          return $this->hasMany('App\VotingOption');
     }

     public function responses(){
          return $this->hasMany('App\VotingResponse');
     }
}
