<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'news';
    protected $fillable = [
        'title',
        'content',
        'quote',
        'author',
        'category_id',
        'date',
        'shift',
        'source_link',
        'meta_id',
		'is_draft'
    ];
    protected $hidden=[
        'category_id',
        'meta_id',
        'created_at',
        'updated_at',
    ];
    protected $appends = [
        'cover',
        'gallery',
        'statistic',
        'depth_links_count',
        'gallery_count'
    ];

	public function getCoverAttribute(){
        if($d=$this->files()->where('type', 'cover')->first()){
            return $d;
        }
        else{
            return '';
        }
    }

	public function getGalleryAttribute(){
        if($d=$this->files()->where('type', 'gallery')->get()){
            return $d;
        }
        else{
            return '';
        }
    }
	public function getStatisticAttribute(){
        return $this->statistics->first();
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function files()
    {
        return $this->morphMany('App\FileManagement', 'fileable');
    }
    public function statistics()
    {
        return $this->morphMany('App\Statistic', 'statable');
    }

    public function depth_links()
    {
        return $this->hasMany('App\DepthLink');
    }
    public function getDepthLinksCountAttribute()
    {
        return $this->depth_links->count();
    }
    public function getGalleryCountAttribute()
    {
        return $this->gallery->count();
    }
    public function voting(){
        return $this->hasOne('App\Voting');
    }
    public function hashtags()
    {
        return $this->morphToMany('App\Hashtag', 'hashtaggable');
    }
    public function topics()
    {
        return $this->belongsToMany('App\Topic');
    }
}
