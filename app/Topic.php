<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'topics';
    protected $fillable = [
        'name',
    ];
    protected $appends = [
        'count',
    ];

	public function getCountAttribute(){
        return $this->news()->count();
    }
    public function news(){
        return $this->belongsToMany('App\News');
    }
}
