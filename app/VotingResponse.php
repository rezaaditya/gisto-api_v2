<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingResponse extends Model
{
     protected $fillable = [
        'voting_option_id',
        'point',
        'user_id',
        'comment',
     ];
}
