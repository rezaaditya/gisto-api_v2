<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsables extends Model
{
    protected $primaryKey='morph_id';
    
    protected $fillable = [
        'position',
        'newsable_id',
        'newsable_type',
        'news_id',
    ];

    public function newsable(){
      return $this->morphTo();
    }
}
