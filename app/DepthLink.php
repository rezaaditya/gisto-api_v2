<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepthLink extends Model
{
    protected $fillable = [
        'id',
        'news_id',
        'name',
        'source_link'
    ];
}
