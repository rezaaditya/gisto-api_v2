<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
		'number',
		'unit',
		'description',
		'statable_id',
		'statable_type',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
