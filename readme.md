# Gisto-api

## Installation
```sh
git clone https://gitlab.com/rezaaditya/gisto-api_v2.git
cp .env.example .env
composer install
```

## Install bower & npm dependencies
```sh
bower install
npm install
```

## Elixir the stuffs
```sh
gulp
```
