@extends('welcome')

@push('stylesheets')
  <!-- iCheck -->
  {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
  <!-- bootstrap-progressbar -->
  {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
  <!-- jVectorMap -->
  {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')
  <script>
  new Vue({
    el:"#content"
  })
  </script>
@endpush
@section('title', 'View News Digest')
  @section('content')
    <div class="row">
      @foreach($data as $key=>$row)
        <panel class="col-md-3" title="{{$key}}">
          <div class="row">
            @foreach($row as $key => $value)
              <div class="col-md-6">
                <btn-app
                href="{{action('Admin\NewsController@digest',[
                  'date'=>$value->get('date'),
                  'shift'=>$value->get('shift'),
                  ])}}"
                  badge-content="{{$value->get('badge-content')}}"
                  icon="{{$value->get('icon')}}"
                  title="{{$value->get('title')}}">
                </btn-app>
              </div>
            @endforeach
          </div>
        </panel>
      @endforeach
    </div>

  @endsection
