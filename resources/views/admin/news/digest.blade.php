@extends('welcome')

@push('stylesheets')
    <!-- iCheck -->
    {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
    <!-- bootstrap-progressbar -->
    {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
    <!-- jVectorMap -->
    {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')
    <script>

    var vm=new Vue({
        el: '#content',
        ready: function() {

            this.$http.get('{!!action('Api\NewsController@index', ['date'=>$request['date'], 'shift'=>$request['shift']])!!}').then((response) => {
                // set data on vm
                this.$set('news', response.json())
            }, (response) => {
            // error callback
            });
          },
        data: {
            news:[]
        }
    })
    </script>
@endpush

@section('content')
    <div class="row">
        <panel class="col-md-12" title="News List" subtitle="20 August 2016">
            <btn-app
                href="{{action('Admin\NewsController@create',[
                    'date'=>$request['date'],
                    'shift'=>$request['shift'],
                    ])}}"
                badge-content="{{$count}} / 6"
                icon="fa-newspaper-o"
                :disabled="{{$count}} > 6"
                title="Add News">
            </btn-app>
            <hr>
            <template v-if="news.data.length > 0">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Cover Image</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="row in news.data">
                            <td>@{{$index+1}}</td>
                            <td><img :src="row.cover.url" :alt="row.title" class="img-responsive" style="height:200px"></td>
                            <td>@{{row.title}}</td>
                            <td>@{{row.category.name}}</td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-default" type="button"><i class="yellow"><i class="fa fa-pencil-square-o"></i></i></button>
                                    <button class="btn btn-default" type="button"><i class="red"><i class="fa fa-trash-o"></i></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </template>
            <template v-if="news.data.length = 0">
                <p>There is no news here! Why don't you add one?</p>
            </template>
        </panel>

    </div>

@endsection
