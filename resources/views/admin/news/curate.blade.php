@extends('welcome')

@push('stylesheets')
  <!-- iCheck -->
  {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
  <!-- bootstrap-progressbar -->
  {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
  <!-- jVectorMap -->
  {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')

  <script
  vm=new Vue({
    el:"#content",
    data:{
      searchText:"",
      curates:[],
      selected:[],
      modalData:{
        index:'0',
        show:false
      }
    },
    ready: function() {
      this.$http.get('{!!action('Api\NewsController@index')!!}').then((response) => {
          // set data on vm
          this.$set('curates', response.json())
      }, (response) => {
      // error callback
      });
    },
    methods: {
      addToCurate: function(index){
        this.selected.data.push(index)
      },
      showModalView:function(index){
        this.modalData.index=index
        this.modalData.show=true
      },
    }
  })
  </script>
@endpush
@section('title', 'View News Digest')
  @section('content')
    <div class="row">
        <panel class="col-md-12">
          <div class="row">
            <div class="col-lg-6">
              <btn-app
                href=""
                :badge-content="selected.length+' / '+curates.data.length"
                icon="fa-paper-plane-o"
                title="Summarize">
              </btn-app>
              <btn-app
                href=""
                :badge-content="'10 / '+curates.count"
                icon="fa-list"
                title="Collect">
              </btn-app>
              <btn-app
                href=""
                icon="fa-remove"
                title="Clear Selection">
              </btn-app>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id="inputSuccess3" placeholder="Search..." v-model="searchText">
                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
              </div>
            </div>
          </div>
        </panel>
        <panel transition="fade" class="animated col-md-3" :title="row.title" v-for="row in curates.data | filterBy searchText in 'title' 'content'" track-by="$index">
          <div class="row">
            @{{{row.content}}}
            <hr>
            <button type="button" class="btn btn-info" @click="showModalView($index)">View More</button>
          </div>
        </panel>
    </div>

    <modal :show.sync="modalData.show" effect="fade">
      <div slot="modal-header" class="modal-header">
        <h4 class="modal-title">
          @{{curates.data[modalData.index].title}}
        </h4>
      </div>
      <div slot="modal-body" class="modal-body">
        @{{{curates.data[modalData.index].content}}}
      </div>
      <div slot="modal-footer" class="modal-footer">
        <button type="button" class="btn btn-default" @click="modalArticle = false">Cancel</button>
        <button type="button" class="btn btn-success" @click="addToCurate(modalData.index)">Curate</button>
      </div>
      </modal>
  @endsection
