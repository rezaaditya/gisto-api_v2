@extends('welcome')

@push('stylesheets')
    <!-- iCheck -->
    {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
    <!-- bootstrap-progressbar -->
    {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
    <!-- jVectorMap -->
    {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
    <style>
    ul.wizard_steps.anchor{
        padding:0;
    }
    .stepContainer{
        height:auto !important;
    }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
    $(document).ready(function() {
        // $('#table').DataTable();
        CKEDITOR.replace( 'editor' );
        $('#wizard').smartWizard();

        $('#wizard_verticle').smartWizard({
            transitionEffect: 'slide',
            fixHeight:null,
            onFinish:onFinishCallback
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
        function onFinishCallback(objs, context){
            // if(validateAllSteps()){
            $('form').submit();
            // }
        }
    });
    </script>
    <script>
    Vue.directive('hashtags', {
        twoWay: true,
        priority: 1000,
        params: ['options'],
        bind: function() {
            var self = this
            $(this.el).select2({
                data: this.params.options,
                tags: true
            })
            .on('change', function() {
                self.set($(self.el).val())
            })
        },
        update: function(value) {
            $(this.el).val(value).trigger('change')
        },
        unbind: function() {
            $(this.el).off().select2('destroy')
        }
    })
    var vm=new Vue({
        el: 'body',
        data:{
            layouts:[],
            selected: '',
            categories: {!!$categories->toJson()!!},
            options:[
                {
                    answer:""
                },
                {
                    answer:""
                },
            ],
            links:[],
            hashtags:[],
            hashtag_options:[
                {
                    id:"1",
                    text:"satuu"
                },
                {
                    id:"2",
                    text:"duaa"
                }
            ],
            title:'',
            image:'',
            gallery:[],
            category_id:'',
            date:'',
            source_link:'',
        },
        methods: {
            addOption: function () {
                this.options.push({
                    answer:""
                })
            },
            removeOption: function (index) {
                this.options.splice(index, 1)
            },

            addHashtag: function () {
                this.hashtags.push({
                    answer:""
                })
            },
            removeHashtag: function (index) {
                this.hashtags.splice(index, 1)
            },

            addLink: function () {
                this.links.push({
                    answer:""
                })
            },
            removeLink: function (index) {
                this.links.splice(index, 1)
            },
            addGallery: function () {
                this.gallery.push({
                    src:""
                })
            },
            removeGallery: function (index) {
                this.gallery.splice(index, 1)
            },
            onFileGalleryChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                return;
                this.createImage(files[0]);
            },
            createImageGallery(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(file);
            },
            removeImageGallery: function (e) {
                this.gallery[index].src = '';
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                return;
                this.createImage(files[0]);
            },
            createImage(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(file);
            },
            removeImage: function (e) {
                this.image = '';
            },
        }
    })

    </script>
@endpush

@section('content')

    {!! Form::model(new App\News, ['action' => ['Admin\NewsController@store'],
        'files' => true
        ]) !!}
        {!! Form::hidden('date', $date) !!}
        {!! Form::hidden('shift', $shift) !!}
        <div class="row">
            <panel class="col-md-12" title="News List" small="20 August 2016">
                <p>This is a basic form wizard example that inherits the colors from the selected scheme.</p>
                <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                        <li><a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                Add Basic Info<br />
                                <small>Step XX description</small>
                            </span>
                        </a></li>
                        <li><a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                Add Quote and Statistic<br />
                                <small>Step XX description</small>
                            </span>
                        </a></li>
                        <li><a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                Add Media and Depth Links<br />
                                <small>Step XX description</small>
                            </span>
                        </a></li>
                        <li><a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                Add Voting<br />
                                <small>Step XX description</small>
                            </span>
                        </a></li>
                    </ul>
                    <div id="step-1">
                        <h2 class="StepTitle">Step 1 Content</h2>
                        <div class="form-group">
                            <label for="title">Title <span class="required">*</span>
                            </label>
                            <input type="text" id="title" class="form-control" name="title" required="">
                        </div>

                        <div class="form-group">
                            <label for="heard">Category:</label>
                            <br>
                            <v-select :value.sync="selected" :options.sync="categories" name="category_id" search clear-button></v-select>
                        </div>

                        <div class="form-group">
                            <label for="title">Cover Image <span class="required">*</span>
                            </label>
                            {!! Form::file('image-main', ['class' => 'form-control', 'v-el:image'=> "", "@change"=>"onFileChange"]) !!}
                            <template v-if="image">
                                <button type="button" @click="removeImage">Remove image</button>
                            </template>
                            <img :src="image" class="img-responsive" alt="http://placehold.it/300x200?text=There+is+no+image!"/>
                        </div>
                        <div class="form-group">
                            <label for="title">Cover Image <span class="required">*</span>
                            </label>
                            <textarea class="form-control" name="content" id="editor"></textarea>
                        </div>

                    </div>
                    <div id="step-2">
                        <div class="row">
                            <div class="col-lg-6">
                                <h2 class="StepTitle">Quote</h2>
                                <div class="form-group">
                                    <label for="title">Title <span class="required">*</span>
                                    </label>
                                    <input type="text" id="title" class="form-control" name="quote" required="">
                                </div>
                                <div class="form-group">
                                    <label for="title">Title <span class="required">*</span>
                                    </label>
                                    <input type="text" id="title" class="form-control" name="author" required="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h2 class="StepTitle">Statistic</h2>
                                <div class="form-group">
                                    <label for="title">Title <span class="required">*</span>
                                    </label>
                                    {!! Form::text('statistic[title]', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="title">Number <span class="required">*</span>
                                    </label>
                                    {!! Form::number('statistic[number]', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="title">Unit <span class="required">*</span>
                                    </label>
                                    {!! Form::text('statistic[unit]', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="title">Description <span class="required">*</span>
                                    </label>
                                    {!! Form::text('statistic[description]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-3">
                        <div class="row">
                            {{-- Media --}}
                            <div class="col-lg-6">
                                <h2 class="StepTitle">Media Gallery</h2>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            {!! Form::label('', 'Add Video Link') !!}
                                            {!! Form::text('video_url', null, ['class' => 'form-control', 'v-model'=> "video_url"]) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-12"><hr></div>
                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <btn-app
                                            @click="addGallery()"
                                            :badge-content="gallery.count"
                                            icon="fa-plus"
                                            title="Add Media">
                                        </btn-app>
                                    </div>

                                    <div class="form-group" v-for="row in gallery" track-by="$index">
                                        <div class="form-group">
                                            {!! Form::label('', 'Image Caption') !!}
                                            <div class="input-group">
                                                {!! Form::text(null, null, ['class' => 'form-control col-md-10', 'v-el:row.src'=> "", ":name"=>"'gallery['+\$index+'][name]'"]) !!}
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger" @click="removeGallery($index)"><i class="fa fa-close"></i></button>
                                                </span>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            {!! Form::label('', 'Media Location') !!}
                                            <br>
                                            <div class="radio-inline">
                                                <label :for="'radio-offline-'+$index">
                                                    <input type="radio" :id="'radio-offline-'+$index" :name="'gallery['+$index+'][is_online]'" value=0 v-model="row.is_online">Upload Image
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label :for="'radio-online-'+$index">
                                                    <input type="radio" :id="'radio-online-'+$index" :name="'gallery['+$index+'][is_online]'" value=1 v-model="row.is_online">External Url
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group" v-if="row.is_online==1">
                                            {!! Form::label('', 'External Url') !!}
                                            {!! Form::text(null, null, ['class' => 'form-control', 'v-model'=> "row.base_url", ":name"=>"'gallery['+\$index+'][base_url]'"]) !!}
                                        </div>
                                        <div class="form-group" v-if="row.is_online==0">
                                            {!! Form::label('', 'Upload Image') !!}
                                            {!! Form::file(null, ['class' => 'form-control', 'v-el:row.src'=> "", "@change"=>"onFileGalleryChange(\$index)", ":name"=>"'gallery['+\$index+'][src]'"]) !!}
                                            <template v-if="image">
                                                <button type="button" @click="removeImageGallery($index)">Remove image</button>
                                            </template>
                                        </div>
                                        <small>Preview</small>
                                        <img :src="row.src" class="img-responsive"/>


                                    </div>

                                </div>
                            </div>
                        </div>
                        {{-- Depth Link --}}
                        <div class="col-lg-6">
                            <h2 class="StepTitle">Depth Links</h2>

                            <btn-app
                            @click="addLink()"
                            :badge-content="links.count"
                            icon="fa-play"
                            title="Add Option">
                        </btn-app>
                        <div class="row" v-for="link in links" track-by="$index">
                            <div class="form-group col-md-6">
                                {!! Form::label('', 'Name') !!}
                                {!! Form::text(null, null, ['class' => 'col-md-6 form-control', ":name"=>"'links['+\$index+'][name]'", "v-model"=>"link.name"]) !!}
                            </div>

                            <div class="form-group col-md-6">
                                {!! Form::label('', 'Source Link') !!}
                                <div class="input-group">
                                    {!! Form::text(null, null, ['placeholder'=>'jete', 'class' => 'form-control', ":name"=>"'links['+\$index+'][source_link]'", "v-model"=>"link.source_link"]) !!}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-danger" @click="removeLink($index)"><i class="fa fa-close"></i></button>
                                    </span>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <div id="step-4">
                <h2 class="StepTitle">Voting</h2>
                <div class="form-group">
                    {!! Form::label('voting[question]', 'Question') !!}
                    {!! Form::text('voting[question]', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label(null, 'Options') !!}
                </div>
                <btn-app
                @click="addOption()"
                :badge-content="links.count"
                icon="fa-plus"
                title="Add Option">
            </btn-app>
            <div class="input-group" v-for="option in options" track-by="$index">
                <input type="text" class="form-control" name="voting[options][][answer]" "v-model"="option.answer">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-danger" @click="removeOption($index)"><i class="fa fa-close"></i></button>
                </span>
            </div>
        </div>

    </div>
    <!-- End SmartWizard Content -->
</panel>
</div>
{!! Form::close() !!}
@endsection
