@extends('welcome')

@push('stylesheets')
    <!-- iCheck -->
    {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
    <!-- bootstrap-progressbar -->
    {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
    <!-- jVectorMap -->
    {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')
    <script>

    var vm=new Vue({
        el: '#content',
        ready: function() {

            this.$http.get('{!!action('Api\TopicController@index')!!}').then((response) => {
                // set data on vm
                this.$set('topic', response.json())
            }, (response) => {
            // error callback
            });
          },
        data: {
            news:[]
        }
    })
    </script>
@endpush

@section('content')
    <div class="row">
        <panel classes="col-md-12" title="News List" subtitle="20 August 2016">
            <btn-app
                href="#"
                icon="fa-newspaper-o"
                title="Add Topic">
            </btn-app>
            <hr>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Count</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="row in topic.data">
                        <td>@{{$index+1}}</td>
                        <td>@{{row.name}}</td>
                        <td>@{{row.count}}</td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-default" type="button"><i class="yellow"><i class="fa fa-pencil-square-o"></i></i></button>
                                <button class="btn btn-default" type="button"><i class="red"><i class="fa fa-trash-o"></i></i></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </panel>

    </div>

@endsection
