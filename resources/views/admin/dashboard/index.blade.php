@extends('welcome')

@push('stylesheets')
    <!-- iCheck -->
    {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
    <!-- bootstrap-progressbar -->
    {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
    <!-- jVectorMap -->
    {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')
    <script>
    new Vue({
        el:"#content"
    })
    </script>
    <script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["20/4", "21/4", "22/4", "23/4", "24/4", "25/4", "26/4"],
        datasets: [{
            label: '# of Average Views',
            data: [86, 123, 100, 87, 68, 99, 111],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

@if($top_hashtag->count())
var topHashtagChart = new Chart($('#topHashtagChart'), {
    type: 'bar',
    data: {
        labels: ["{{$top_hashtag[0]->name}}", "{{$top_hashtag[1]->name}}", "{{$top_hashtag[2]->name}}", "{{$top_hashtag[3]->name}}", "{{$top_hashtag[4]->name}}"],
        datasets: [{
            label: '# of usage',
            data: [{{$top_hashtag[0]->count}}, {{$top_hashtag[1]->count}}, {{$top_hashtag[2]->count}}, {{$top_hashtag[3]->count}}, {{$top_hashtag[4]->count}}],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
@endif
</script>
@endpush

@section('content')
    <div class="row top_tiles">
        <tile-stats-v1
            icon="fa-user"
            count="{{$data['category_count']}}"
            title="Categories"
            subtitle="on last 7 days"
        ></tile-stats-v1>
        <tile-stats-v1
            icon="fa-newspaper-o"
            count="{{$data['topic_count']}}"
            title="Topics"
            subtitle=""
        ></tile-stats-v1>
        <tile-stats-v1
            icon="fa-user"
            count="{{$data['hashtag_count']}}"
            title="Hashtags"
            subtitle="on last 7 days"
        ></tile-stats-v1>
        <tile-stats-v1
            icon="fa-newspaper-o"
            count="{{$data['library_count']}}"
            title="Published Libraries"
            subtitle=""
        ></tile-stats-v1>
    </div>
    <div class="row top_tiles">
        <tile-stats-v1
            icon="fa-newspaper-o"
            count="{{$data['news_count']}}"
            title="News Count"
            subtitle="From Last Week"
        ></tile-stats-v1>
        <tile-stats-v1
            icon="fa-newspaper-o"
            count=100
            title="Average View per News"
            subtitle="From Last Week"
        ></tile-stats-v1>
    </div>
    <div class="row">

        <panel title="Average Views per article" class="col-lg-4">
            <canvas id="myChart" height="300"></canvas>
        </panel>
        <panel title="Top 5 Hashtags this week" class="col-lg-4">
            <canvas id="topHashtagChart" height="300"></canvas>
        </panel>
        <panel title="surKEJO">
            <ul>
                <li>HAIYO</li>
                <li>HAIYO</li>
                <li>HAIYO</li>
                <li>HAIYO</li>
                <li>HAIYO</li>
            </ul>
        </panel>
    </div>
@endsection
