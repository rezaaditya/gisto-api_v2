@extends('welcome')

@push('stylesheets')
    <!-- iCheck -->
    {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
    <!-- bootstrap-progressbar -->
    {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
    <!-- jVectorMap -->
    {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')
    <script>

    var vm=new Vue({
        el: '#content',
        ready: function() {
            this.initLibrary()
          },
        data: {
            Library:[],
            modalLibrary:false,
            modal:{
                name:''
            }
            ,
            selected:{
                idx:null,
                value:null
            }
        },
      methods: {
        initLibrary: function(){
            this.$http.get('{!!action('Api\LibraryController@index')!!}').then((response) => {
                // set data on vm
                this.$set('Library', response.json())
            }, (response) => {
            // error callback
            });
        },
        modalAddLibrary: function () {
            this.$set('modal.name', '')
            this.$set('modalLibrary', true)
        },
        addLibrary: function () {
          var text = this.modal.name.trim()
          if (text) {
            // this.Library.data.push({ name: this.modal.name })
            this.newTodo = ''
            this.$http.post('{!!action('Api\LibraryController@store')!!}', {name:this.modal.name}).then((response) => {
                vm.initLibrary()
                swal({
                    title: 'Success',
                    text: 'Library has been added!',
                    type: 'success',
                    timer:2000
                });
            }, (response) => {
            // error callback
            });
          }
        },
        editLibrary: function (index) {
            if(this.selected.idx == index){
                this.$set('selected.idx',null)
                this.$set('selected.value',null)
            }
            else {
                this.$set('selected.idx',index)
                this.$set('selected.value',this.Library.data[index].name)
            }
        },
        confirmEditLibrary: function (index) {
            this.$set('Library.data['+index+'].name',this.selected.value)

            this.$http.put('{!! url('api/Library/')."/" !!}'+this.Library.data[index].id, {name:this.selected.value}).then((response) => {
                swal({
                    title: 'Success',
                    text: 'Library has been updated!',
                    type: 'success',
                    timer:2000
                });
                vm.initLibrary()
            }, (response) => {
            // error callback
            });
            this.$set('selected.idx',null)
            this.$set('selected.value',null)
        },
        removeLibrary: function (index) {
            swal({
              title: 'Are you sure?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, keep it',
            }).then(function() {
                vm.$http.delete('{!! url('api/Library/')."/" !!}'+vm.$data.Library.data[index].id).then((response) => {
                    // set data on vm
                    swal({
                        title: 'Deleted',
                        text: 'Library has been  deleted!',
                        type: 'success',
                        timer:2000
                    }).done();
                    vm.$data.Library.data.splice(index, 1)
                    vm.initLibrary()

                }, (response) => {
                // error callback
                });

            }, function(dismiss) {
              // dismiss can be 'cancel', 'overlay', 'close', 'timer'
              if (dismiss === 'cancel') {
                  swal({
                      title: 'Canceled',
                      text: 'Delete has been canceled!',
                      type: 'error',
                      timer:2000
                  }).done();
              }
            });
        }
      }
    })
    </script>
@endpush

@section('content')
    <modal :show.sync="modalLibrary" effect="fade">
        <div slot="modal-header" class="modal-header">
      <h4 class="modal-title">
        Add Library
      </h4>
    </div>
    <div slot="modal-body" class="modal-body">
        <label for="">Library Name</label>
        <input type="text" class="form-control" v-model="modal.name" @keyup.enter="addLibrary()" >
    </div>
    <div slot="modal-footer" class="modal-footer">
      <button type="button" class="btn btn-default" @click="modalLibrary = false">Cancel</button>
      <button type="button" class="btn btn-success" @click="addLibrary()">Add</button>
    </div>
    </modal>

    <div class="row">
        <panel class="col-md-12" title="Library">
            <btn-app
                href="#"
                icon="fa-files-o"
                @click="modalAddLibrary()"
                title="Add Library">
            </btn-app>
            <hr>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Count</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="row in Library.data">
                        <td>@{{$index+1}}</td>
                        <td>
                            <template v-if="selected.idx != $index">@{{row.name}}</template>
                            <div class="input-group" v-else>
                                <input type="text" class="form-control" v-model="selected.value">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success" @click="confirmEditLibrary($index)"><i class="fa fa-check"></i></button>
                                    <button type="button" class="btn btn-danger" @click="editLibrary($index)"><i class="fa fa-close"></i></button>
                                </span>
                              </div>
                        </td>
                        <td>@{{row.count}}</td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" @click="editLibrary($index)"><i class="yellow"><i class="fa fa-pencil-square-o"></i></i></button>
                                <button class="btn btn-default" type="button" @click="removeLibrary($index)"><i class="red"><i class="fa fa-trash-o"></i></i></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </panel>

    </div>

@endsection
