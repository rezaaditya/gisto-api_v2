@extends('welcome')

@push('stylesheets')
    <!-- iCheck -->
    {{-- <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> --}}
    <!-- bootstrap-progressbar -->
    {{-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> --}}
    <!-- jVectorMap -->
    {{-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> --}}
@endpush

@push('scripts')
    <script>

    var vm=new Vue({
        el: '#content',
        ready: function() {
            this.initCategory()
          },
        data: {
            category:[],
            modalCategory:false,
            modal:{
                name:''
            }
            ,
            selected:{
                idx:null,
                value:null
            }
        },
      methods: {
        initCategory: function(){
            this.$http.get('{!!action('Api\CategoryController@index')!!}').then((response) => {
                // set data on vm
                this.$set('category', response.json())
            }, (response) => {
            // error callback
            });
        },
        modalAddCategory: function () {
            this.$set('modal.name', '')
            this.$set('modalCategory', true)
        },
        addCategory: function () {
          var text = this.modal.name.trim()
          if (text) {
            // this.category.data.push({ name: this.modal.name })
            this.newTodo = ''
            this.$http.post('{!!action('Api\CategoryController@store')!!}', {name:this.modal.name}).then((response) => {
                vm.initCategory()
                swal({
                    title: 'Success',
                    text: 'Category has been added!',
                    type: 'success',
                    timer:2000
                });
            }, (response) => {
            // error callback
            });
          }
        },
        editCategory: function (index) {
            if(this.selected.idx == index){
                this.$set('selected.idx',null)
                this.$set('selected.value',null)
            }
            else {
                this.$set('selected.idx',index)
                this.$set('selected.value',this.category.data[index].name)
            }
        },
        confirmEditCategory: function (index) {
            this.$set('category.data['+index+'].name',this.selected.value)

            this.$http.put('{!! url('api/category/')."/" !!}'+this.category.data[index].id, {name:this.selected.value}).then((response) => {
                swal({
                    title: 'Success',
                    text: 'Category has been updated!',
                    type: 'success',
                    timer:2000
                });
                vm.initCategory()
            }, (response) => {
            // error callback
            });
            this.$set('selected.idx',null)
            this.$set('selected.value',null)
        },
        removeCategory: function (index) {
            swal({
              title: 'Are you sure?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, keep it',
            }).then(function() {
                vm.$http.delete('{!! url('api/category/')."/" !!}'+vm.$data.category.data[index].id).then((response) => {
                    // set data on vm
                    swal({
                        title: 'Deleted',
                        text: 'Category has been  deleted!',
                        type: 'success',
                        timer:2000
                    }).done();
                    vm.$data.category.data.splice(index, 1)
                    vm.initCategory()

                }, (response) => {
                // error callback
                });

            }, function(dismiss) {
              // dismiss can be 'cancel', 'overlay', 'close', 'timer'
              if (dismiss === 'cancel') {
                  swal({
                      title: 'Canceled',
                      text: 'Delete has been canceled!',
                      type: 'error',
                      timer:2000
                  }).done();
              }
            });
        }
      }
    })
    </script>
@endpush

@section('content')
    <modal :show.sync="modalCategory" effect="fade">
        <div slot="modal-header" class="modal-header">
      <h4 class="modal-title">
        Add Category
      </h4>
    </div>
    <div slot="modal-body" class="modal-body">
        <label for="">Category Name</label>
        <input type="text" class="form-control" v-model="modal.name" @keyup.enter="addCategory()" >
    </div>
    <div slot="modal-footer" class="modal-footer">
      <button type="button" class="btn btn-default" @click="modalCategory = false">Cancel</button>
      <button type="button" class="btn btn-success" @click="addCategory()">Add</button>
    </div>
    </modal>

    <div class="row">
        <panel class="col-md-12" title="Category">
            <btn-app
                href="#"
                icon="fa-newspaper-o"
                @click="modalAddCategory()"
                title="Add Category">
            </btn-app>
            <hr>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Count</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="row in category.data">
                        <td>@{{$index+1}}</td>
                        <td>
                            <template v-if="selected.idx != $index">@{{row.name}}</template>
                            <div class="input-group" v-else>
                                <input type="text" class="form-control" v-model="selected.value">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success" @click="confirmEditCategory($index)"><i class="fa fa-check"></i></button>
                                    <button type="button" class="btn btn-danger" @click="editCategory($index)"><i class="fa fa-close"></i></button>
                                </span>
                              </div>
                        </td>
                        <td>@{{row.count}}</td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" @click="editCategory($index)"><i class="yellow"><i class="fa fa-pencil-square-o"></i></i></button>
                                <button class="btn btn-default" type="button" @click="removeCategory($index)"><i class="red"><i class="fa fa-trash-o"></i></i></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </panel>

    </div>

@endsection
