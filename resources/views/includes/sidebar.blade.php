@inject('sidebar', 'App\Services\Sidebar')
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{url('admin')}}" class="site_title"><i class="fa fa-paw"></i> <span>{{$sidebar->get()['title']}}</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            @foreach($sidebar->get()['items'] as $row)
                <div class="menu_section">
                    <h3>{{ $row['header'] }}</h3>
                    <ul class="nav side-menu">
                        @foreach($row['items'] as $item)
                            @if(array_key_exists('sub_items',$item))
                                <li><a><i class="fa {{ $item['fa-icon'] }}"></i>{{ $item['name'] }}<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        @foreach($item['sub_items'] as $sub_item)
                                            <li><a href="{{ action($sub_item['controller']) }}">{{ $sub_item['name'] }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ action($item['controller']) }}"><i class="fa {{ $item['fa-icon'] }}"></i>{{ $item['name'] }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
