<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gentellela Alela! | </title>

        <!-- Bootstrap -->
        {{-- <link href="{{asset('css/app.css')}}" rel="stylesheet"> --}}
        <link href="{{asset('bower_components/css/all.css')}}" rel="stylesheet">
        <!-- Font Awesome -->
        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

            </div>
        </div>

        <script>
            window.Laravel = { csrfToken: '{{ csrf_token() }}' };
        </script>
        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('bower_components/js/all.js')}}"></script>

        @stack('scripts')

    </body>
</html>
